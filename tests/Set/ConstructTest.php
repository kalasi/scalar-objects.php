<?php
namespace tests\Set;

use Boolean;
use Double;
use Integer;
use Map;
use Set;
use Str;
use tests\Test;
use Vector;

/**
 * Class ConstructTest
 */
final class ConstructTest extends Test
{
    public function testEmpty()
    {
        $this->check([], (new Set)->parse());
        $this->check([], (new Set([]))->parse());
    }

    public function testInteger()
    {
        $this->check([1], (new Set([1]))->parse());
        $this->check([1], (new Set([new Integer(1)]))->parse());
    }

    public function testStr()
    {
        $this->check(['test'], (new Set(['test']))->parse());
        $this->check(['test'], (new Set([new Str('test')]))->parse());
    }

    public function testInvalid()
    {
        // Check that an invalid type such as a null sets the Set to nothing.
        $this->check(new Set, new Set([null]));
        $this->check([], (new Set([null]))->parse());

        // Check that passing primitive arrays sets nothing.
        $this->check(new Set, new Set([[]]));

        // Check that passing primitive booleans sets nothing.
        $this->check(new Set, new Set([false]));

        // Check that passing a Boolean sets nothing.
        $this->check(new Set, new Set([new Boolean]));

        // Check that passing primitive doubles sets nothing.
        $this->check(new Set, new Set([3.0]));

        // Check that passing a Double sets nothing.
        $this->check(new Set, new Set([new Double]));

        // Check that passing a Map sets nothing.
        $this->check(new Set, new Set([new Map]));

        // Check that passing an Object sets nothing.
        $this->check(new Set, new Set([new \stdClass]));

        // Check that passing a Set sets nothing.
        $this->check(new Set, new Set([new Set]));

        // Check that passing a Vector sets nothing.
        $this->check(new Set, new Set([new Vector]));
    }
}
