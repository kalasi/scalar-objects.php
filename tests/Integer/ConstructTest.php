<?php
namespace tests\Integer;

use Double;
use Integer;
use Str;
use tests\Test;

/**
 * class ConstructTest
 */
final class ConstructTest extends Test
{
    public function testArguments()
    {
        $this->check(0, (new Integer)->parse());
        $this->check(0, (new Integer(0))->parse());
    }

    public function testDouble()
    {
        $this->check(42, (new Integer(42.1337))->parse());
        $this->check(-42, (new Integer(-42.1337))->parse());
        $this->check(0, (new Integer(new Double(0.0)))->parse());
        $this->check(42, (new Integer(new Double(42.1337)))->parse());
        $this->check(-42, (new Integer(new Double(-42.1337)))->parse());
    }

    public function testInteger()
    {
        $this->check(42, (new Integer(42))->parse());
        $this->check(-42, (new Integer(-42))->parse());
        $this->check(0, (new Integer(0))->parse());
        $this->check(42, (new Integer(new Integer(42)))->parse());
        $this->check(-42, (new Integer(new Integer(-42)))->parse());
    }

    public function testString()
    {
        $this->check(7, (new Integer('7'))->parse());
        $this->check(-7, (new Integer('-7'))->parse());
        $this->check(0, (new Integer(''))->parse());
        $this->check(7, (new Integer(new Str('7')))->parse());
        $this->check(-7, (new Integer(new Str('-7')))->parse());
    }
}
