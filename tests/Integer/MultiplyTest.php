<?php
namespace tests\Integer;

use Integer;
use tests\Test;

/**
 * class MultiplyTest
 */
final class MultiplyTest extends Test
{
    public function testMultiplyNegative()
    {
        $integer = (new Integer(4))->multiply(-4, true);
        $this->check(-16, $integer);

        $integer = (new Integer(4))->multiply(-4);
        $this->check(new Integer(-16), $integer);
    }

    public function testMultiplyPositive()
    {
        $integer = (new Integer(4))->multiply(4, true);
        $this->check(16, $integer);

        $integer = (new Integer(4))->multiply(4);
        $this->check(new Integer(16), $integer);
    }

    public function testMultiplyZero()
    {
        $integer = (new Integer(4))->multiply(0, true);
        $this->check(0, $integer);

        $integer = (new Integer(4))->multiply(0);
        $this->check(new Integer(0), $integer);
    }
}
