<?php
namespace tests\Integer;

use Integer;
use Str;
use tests\Test;

/**
 * class BinTest
 */
final class BinTest extends Test
{
    public function testDecbin()
    {
        $bin = (new Integer(42))->decbin()->parse();
        $this->check('101010', $bin);

        $bin = (new Integer(42))->decbin(true);
        $this->check('101010', $bin);

        $bin = (new Integer(42))->decbin();
        $this->check(new Str('101010'), $bin);
    }

    public function testToBin()
    {
        $bin = (new Integer(42))->toBin()->parse();
        $this->check('101010', $bin);

        $bin = (new Integer(42))->toBin(true);
        $this->check('101010', $bin);

        $bin = (new Integer(42))->toBin();
        $this->check(new Str('101010'), $bin);
    }
}
