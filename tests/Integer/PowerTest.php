<?php
namespace tests\Integer;

use Integer;
use tests\Test;

/**
 * class PowerTest
 */
final class PowerTest extends Test
{
    /**
     * The exponent for tests to use for exponential powers.
     *
     * @var int
     */
    private $exponent = 0;

    /**
     * Called on construct.
     */
    public function __construct()
    {
        $this->exponent = 2;
    }

    public function testPowNegative()
    {
        $integer = (new Integer(-3))->pow($this->exponent);

        $this->check(new Integer(9), $integer);
        $this->check(9, $integer->parse());
    }

    public function testPowPositive()
    {
        $integer = (new Integer(3))->pow($this->exponent);

        $this->check(new Integer(9), $integer);
        $this->check(9, $integer->parse());
    }

    public function testPowZero()
    {
        $integer = (new Integer(0))->pow($this->exponent);

        $this->check(new Integer(0), $integer);
        $this->check(0, $integer->parse());
    }

    public function testPowerNegative()
    {
        $integer = (new Integer(-3))->power($this->exponent);

        $this->check(new Integer(9), $integer);
        $this->check(9, $integer->parse());
    }

    public function testPowerPositive()
    {
        $integer = (new Integer(3))->power($this->exponent);

        $this->check(new Integer(9), $integer);
        $this->check(9, $integer->parse());
    }

    public function testPowerZero()
    {
        $integer = (new Integer(0))->power($this->exponent);

        $this->check(new Integer(0), $integer);
        $this->check(0, $integer->parse());
    }
}
