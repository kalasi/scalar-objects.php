<?php
namespace tests\Integer;

use Integer;
use tests\Test;

/**
 * class AbsoluteTest
 */
final class AbsoluteTest extends Test
{
    public function testNegative()
    {
        $integer = (new Integer(-42))->abs()->parse();
        $this->check(42, $integer);

        $integer = (new Integer(-42))->absolute()->parse();
        $this->check(42, $integer);
    }

    public function testPositive()
    {
        $integer = (new Integer(42))->abs()->parse();
        $this->check(42, $integer);

        $integer = (new Integer(42))->absolute()->parse();
        $this->check(42, $integer);
    }

    public function testZero()
    {
        $integer = (new Integer)->abs()->parse();
        $this->check(0, $integer);

        $integer = (new Integer)->absolute()->parse();
        $this->check(0, $integer);
    }
}
