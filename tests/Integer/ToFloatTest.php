<?php
namespace tests\Integer;

use Double;
use Integer;
use tests\Test;

/**
 * class ToFloatTest
 */
final class ToFloatTest extends Test
{
    public function testToFloat()
    {
        $float = (new Integer(4))->toFloat(true);
        $this->check(4.0, $float);

        $float = (new Integer(0))->toFloat(true);
        $this->check(0.0, $float);

        $float = (new Integer(-4))->toFloat(true);
        $this->check(-4.0, $float);

        $float = (new Integer(4))->toFloat();
        $this->check(new Double(4.0), $float);

        $float = (new Integer(0))->toFloat();
        $this->check(new Double(0.0), $float);

        $float = (new Integer(-4))->toFloat();
        $this->check(new Double(-4.0), $float);
    }
}
