<?php
namespace tests\Integer;

use Integer;
use ScalarObjects\Exceptions\DivideByZero;
use tests\Test;

/**
 * class DivideTest
 */
final class DivideTest extends Test
{
    public function testDivideByNegative()
    {
        $integer = (new Integer(12))->divideBy(-3, true);
        $this->check(-4, $integer);

        $integer = (new Integer(12))->divideBy(-3);
        $this->check(new Integer(-4), $integer);
    }

    public function testDivideByPositive()
    {
        $integer = (new Integer(12))->divideBy(3, true);
        $this->check(4, $integer);

        $integer = (new Integer(12))->divideBy(3);
        $this->check(new Integer(4), $integer);
    }

    public function testDivideByZero()
    {
        $integer = (new Integer(12));

        $exception = '';

        try {
            $integer->divideBy(0, true);
        } catch (DivideByZero $e) {
            $exception = 'caught';
        }

        $this->check('caught', $exception);

        $integer = (new Integer(12));

        $exception = '';

        try {
            $integer->divideBy(0);
        } catch (DivideByZero $e) {
            $exception = 'caught';
        }

        $this->check('caught', $exception);
    }

    public function testDivideFromNegative()
    {
        $integer = (new Integer(3))->divideFrom(-12, true);
        $this->check(-4, $integer);

        $integer = (new Integer(3))->divideFrom(-12);
        $this->check(new Integer(-4), $integer);
    }

    public function testDivideFromPositive()
    {
        $integer = (new Integer(3))->divideFrom(12, true);
        $this->check(4, $integer);

        $integer = (new Integer(3))->divideFrom(12);
        $this->check(new Integer(4), $integer);
    }

    public function testDivideFromZero()
    {
        $integer = (new Integer(12))->divideFrom(0, true);
        $this->check(0, $integer);

        $integer = (new Integer(12))->divideFrom(0);
        $this->check(new Integer(0), $integer);
    }

    public function testDivideNegative()
    {
        $integer = (new Integer(12))->divide(-3, true);
        $this->check(-4, $integer);

        $integer = (new Integer(12))->divide(-3);
        $this->check(new Integer(-4), $integer);
    }

    public function testDividePositive()
    {
        $integer = (new Integer(12))->divide(3, true);
        $this->check(4, $integer);

        $integer = (new Integer(12))->divide(3);

        $this->check(new Integer(4), $integer);
    }

    public function testDivideZero()
    {
        $integer = (new Integer(12));

        $exception = '';

        try {
            $integer->divide(0, true);
        } catch (DivideByZero $e) {
            $exception = 'caught';
        }

        $this->check('caught', $exception);

        $integer = (new Integer(12));

        $exception = '';

        try {
            $integer->divide(0);
        } catch (DivideByZero $e) {
            $exception = 'caught';
        }

        $this->check('caught', $exception);
    }
}
