<?php
namespace tests\Integer;

use Integer;
use tests\Test;

/**
 * class SubtractTest
 */
final class SubtractTest extends Test
{
    public function testSubtractNegative()
    {
        $integer = (new Integer(4))->subtract(-2, true);
        $this->check(6, $integer);

        $integer = (new Integer(4))->subtract(-2);
        $this->check(new Integer(6), $integer);
    }

    public function testSubtractPositive()
    {
        $integer = (new Integer(4))->subtract(2, true);
        $this->check(2, $integer);

        $integer = (new Integer(4))->subtract(2);
        $this->check(new Integer(2), $integer);
    }

    public function testSubtractZero()
    {
        $integer = (new Integer(4))->subtract(0, true);
        $this->check(4, $integer);

        $integer = (new Integer(4))->subtract(0);
        $this->check(new Integer(4), $integer);
    }

    public function testSubtractByNegative()
    {
        $integer = (new Integer(4))->subtractBy(-2, true);
        $this->check(6, $integer);

        $integer = (new Integer(4))->subtractBy(-2);
        $this->check(new Integer(6), $integer);
    }

    public function testSubtractByPositive()
    {
        $integer = (new Integer(4))->subtractBy(2, true);
        $this->check(2, $integer);

        $integer = (new Integer(4))->subtractBy(2);
        $this->check(new Integer(2), $integer);
    }

    public function testSubtractByZero()
    {
        $integer = (new Integer(4))->subtractBy(0, true);
        $this->check(4, $integer);

        $integer = (new Integer(4))->subtractBy(0);
        $this->check(new Integer(4), $integer);
    }
}
