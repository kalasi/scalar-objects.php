<?php
namespace tests\Integer;

use Double;
use Integer;
use tests\Test;

/**
 * class ToDoubleTest
 */
final class ToDoubleTest extends Test
{
    public function testToDouble()
    {
        $double = (new Integer(4))->toDouble(true);
        $this->check(4.0, $double);

        $double = (new Integer(0))->toDouble(true);
        $this->check(0.0, $double);

        $double = (new Integer(-4))->toDouble(true);
        $this->check(-4.0, $double);

        $double = (new Integer(4))->toDouble();
        $this->check(new Double(4.0), $double);

        $double = (new Integer(0))->toDouble();
        $this->check(new Double(0.0), $double);

        $double = (new Integer(-4))->toDouble();
        $this->check(new Double(-4.0), $double);
    }
}
