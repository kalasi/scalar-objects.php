<?php
namespace tests\Integer;

use Integer;
use Str;
use tests\Test;

/**
 * class ToStringTest
 */
final class ToStringTest extends Test
{
    public function testToStringCast()
    {
        $string = (string) new Integer(4);
        $this->check('4', $string);

        $string = (string) new Integer(-4);
        $this->check('-4', $string);
    }

    public function testToStringPrimitive()
    {
        $string = (new Integer(4))->toString(true);
        $this->check('4', $string);

        $string = (new Integer(-4))->toString(true);
        $this->check('-4', $string);
    }

    public function testToStringScalar()
    {
        $string = (new Str(4))->toString();
        $this->check(new Str('4'), $string);

        $string = (new Str(-4))->toString();
        $this->check(new Str('-4'), $string);
    }
}
