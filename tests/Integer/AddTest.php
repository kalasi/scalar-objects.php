<?php
namespace tests\Integer;

use Integer;
use tests\Test;

/**
 * class AddTest
 */
final class AddTest extends Test
{
    public function testAddByNegative()
    {
        $integer = (new Integer(42))->addBy(-13);
        $this->check(new Integer(29), $integer);

        $integer = (new Integer(42))->addBy(-13, true);
        $this->check(29, $integer);
    }

    public function testAddByPositive()
    {
        $integer = (new Integer(42))->addBy(13);
        $this->check(new Integer(55), $integer);

        $integer = (new Integer(42))->addBy(13, true);
        $this->check(55, $integer);
    }

    public function testAddByZero()
    {
        $integer = (new Integer(42))->addBy(0);
        $this->check(new Integer(42), $integer);

        $integer = (new Integer(42))->addBy(0, true);
        $this->check(42, $integer);
    }

    public function testAddNegative()
    {
        $integer = (new Integer(42))->add(-13);
        $this->check(new Integer(29), $integer);

        $integer = (new Integer(42))->add(-13, true);
        $this->check(29, $integer);
    }

    public function testAddPositive()
    {
        $integer = (new Integer(42))->add(13);
        $this->check(new Integer(55), $integer);

        $integer = (new Integer(42))->add(13, true);
        $this->check(55, $integer);
    }

    public function testAddZero()
    {
        $integer = (new Integer(42))->add(0);
        $this->check(new Integer(42), $integer);

        $integer = (new Integer(42))->add(0, true);
        $this->check(42, $integer);
    }
}
