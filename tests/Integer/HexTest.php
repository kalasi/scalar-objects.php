<?php
namespace tests\Integer;

use Integer;
use Str;
use tests\Test;

/**
 * class HexTest
 */
final class HexTest extends Test
{
    public function testDechex()
    {
        $hex = (new Integer(42))->dechex()->parse();
        $this->check('2a', $hex);

        $hex = (new Integer(42))->dechex(true);
        $this->check('2a', $hex);

        $hex = (new Integer(42))->dechex();
        $this->check(new Str('2a'), $hex);
    }

    public function testToHex()
    {
        $hex = (new Integer(42))->toHex()->parse();
        $this->check('2a', $hex);

        $hex = (new Integer(42))->toHex(true);
        $this->check('2a', $hex);

        $hex = (new Integer(42))->toHex();
        $this->check(new Str('2a'), $hex);
    }
}
