<?php
namespace tests\Integer;

use Integer;
use Str;
use tests\Test;

/**
 * class OctTest
 */
final class OctTest extends Test
{
    public function testDecoct()
    {
        $string = (new Integer(42))->decoct()->parse();
        $this->check('52', $string);

        $string = (new Integer(42))->decoct(true);
        $this->check('52', $string);

        $string = (new Integer(42))->decoct(true);
        $this->check('52', $string);
    }

    public function testToOct()
    {
        $string = (new Integer(42))->toOct()->parse();
        $this->check('52', $string);

        $string = (new Integer(42))->toOct(true);
        $this->check('52', $string);

        $string = (new Integer(42))->toOct(true);
        $this->check('52', $string);

        $string = (new Integer(42))->toOct();
        $this->check(new Str('52'), $string);
    }
}
