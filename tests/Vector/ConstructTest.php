<?php
namespace tests\Vector;

use Boolean;
use tests\Test;
use Vector;

/**
 * Class ConstructTest
 */
final class ConstructTest extends Test
{
    public function testEmpty()
    {
        $this->check([], (new Vector([]))->parse());
        $this->check([], (new Vector)->parse());
    }

    public function testFailures()
    {
        $ex = '';

        try {
            (new Vector(true));
        } catch (\Exception $e) {
            $ex = $e->getCode();
        }

        $this->check(2, $ex);

        try {
            (new Vector(false));
        } catch (\Exception $e) {
            $ex = $e->getCode();
        }

        $this->check(2, $ex);

        try {
            (new Vector(42));
        } catch (\Exception $e) {
            $ex = $e->getCode();
        }

        $this->check(2, $ex);

        try {
            (new Vector([0 => 'test']));
        } catch (\Exception $e) {
            $ex = $e->getCode();
        }

        $this->check(2, $ex);

        try {
            (new Vector(new Boolean(true)));
        } catch (\Exception $e) {
            $ex = $e->getCode();
        }

        $this->check(2, $ex);
    }

    public function testHasBoolean()
    {
        $this->check([0 => false], (new Vector([0 => false]))->parse());
    }

    public function testHasInteger()
    {
        $this->check([0 => 1], (new Vector([0 => 1]))->parse());
    }
}
