<?php
namespace tests\Vector;

use Str;
use tests\Test;
use Vector;

/**
 * Class ToStringTest
 */
final class ToStringTest extends Test
{
    public function testAll()
    {
        $vector = (string) (new Vector([]))->toString();
        $this->check('', $vector);
        $vector = (string) (new Vector([0 => -1]))->toString();
        $this->check('0: -1', $vector);
        $vector = (string) (new Vector([0 => 1]))->toString();
        $this->check('0: 1', $vector);

        $vector = (new Vector([]))->toString(true);
        $this->check('', $vector);
        $vector = (new Vector([0 => -1]))->toString(true);
        $this->check('0: -1', $vector);
        $vector = (new Vector([0 => 1]))->toString(true);
        $this->check('0: 1', $vector);

        $vector = (new Vector([]))->toString();
        $this->check(new Str, $vector);
        $vector = (new Vector([0 => -1]))->toString();
        $this->check(new Str('0: -1'), $vector);
        $vector = (new Vector([0 => 1]))->toString();
        $this->check(new Str('0: 1'), $vector);
    }
}
