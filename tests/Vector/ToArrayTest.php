<?php
namespace tests\Vector;

use tests\Test;
use Vector;

/**
 * Class ToArrayTest
 */
final class ToArrayTest extends Test
{
    public function testAll()
    {
        $this->check([], (new Vector([]))->toArray());
        $this->check([0 => -1], (new Vector([0 => -1]))->toArray());
        $this->check([0 => 1], (new Vector([0 => 1]))->toArray());
    }
}
