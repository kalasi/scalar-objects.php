<?php
namespace tests\Map;

use Map;
use tests\Test;

/**
 * Class ConstructTest
 */
final class ConstructTest extends Test
{
    public function testEmpty()
    {
        $this->check([], (new Map([]))->parse());
        $this->check([], (new Map)->parse());
    }

    public function testValid()
    {
        $this->check([0 => 't1', 't2' => 0], (new Map([0 => 't1', 't2' => 0]))->parse());
    }
}
