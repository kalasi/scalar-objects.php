<?php
namespace tests\Boolean;

use Boolean;
use Integer;
use tests\Test;

/**
 * class ToIntTest
 */
final class ToIntTest extends Test
{
    public function testIntegerFalse()
    {
        $this->check(0, (new Boolean(false))->toInt(true));
        $this->check(new Integer(0), (new Boolean(false))->toInt());
    }

    public function testIntegerTrue()
    {
        $this->check(1, (new Boolean(true))->toInt(true));
        $this->check(new Integer(1), (new Boolean(true))->toInt());
    }
}
