<?php
namespace tests\Boolean;

use Boolean;
use Integer;
use tests\Test;

/**
 * class CompareToTest
 */
final class CompareToTest extends Test
{
    public function testFalseToFalse()
    {
        $integer = (new Boolean(false))->compareTo(false);
        $this->check(new Integer(0), $integer);

        $integer = (new Boolean(false))->compareTo(false, true);
        $this->check(0, $integer);

        $integer = (new Boolean(false))->compareTo(new Boolean(false));
        $this->check(new Integer(0), $integer);

        $integer = (new Boolean(false))->compareTo(new Boolean(false), true);
        $this->check(0, $integer);
    }

    public function testFalseToTrue()
    {
        $integer = (new Boolean(false))->compareTo(true);
        $this->check(new Integer(-1), $integer);

        $integer = (new Boolean(false))->compareTo(true, true);
        $this->check(-1, $integer);

        $integer = (new Boolean(false))->compareTo(true);
        $this->check(new Integer(-1), $integer);

        $integer = (new Boolean(false))->compareTo(new Boolean(true), true);
        $this->check(-1, $integer);
    }

    public function testTrueToFalse()
    {
        $integer = (new Boolean(true))->compareTo(false);
        $this->check(new Integer(1), $integer);

        $integer = (new Boolean(true))->compareTo(false, true);
        $this->check(1, $integer);

        $integer = (new Boolean(true))->compareTo(new Boolean(false));
        $this->check(new Integer(1), $integer);

        $integer = (new Boolean(true))->compareTo(new Boolean(false), true);
        $this->check(1, $integer);
    }

    public function testTrueToTrue()
    {
        $integer = (new Boolean(true))->compareTo(true);
        $this->check(new Integer(0), $integer);

        $integer = (new Boolean(true))->compareTo(true, true);
        $this->check(0, $integer);

        $integer = (new Boolean(true))->compareTo(new Boolean(true));
        $this->check(new Integer(0), $integer);

        $integer = (new Boolean(true))->compareTo(new Boolean(true), true);
        $this->check(0, $integer);
    }
}
