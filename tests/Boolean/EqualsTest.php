<?php
namespace tests\Boolean;

use Boolean;
use tests\Test;

/**
 * class EqualsTest
 */
final class EqualsTest extends Test
{
    public function testFalseFalse()
    {
        $boolean = (new Boolean(false))->equals(false, true);
        $this->check(true, $boolean);

        $boolean = (new Boolean(false))->equals(false);
        $this->check(new Boolean(true), $boolean);
    }

    public function testFalseTrue()
    {
        $boolean = (new Boolean(false))->equals(true, true);
        $this->check(false, $boolean);

        $boolean = (new Boolean(false))->equals(true);
        $this->check(new Boolean(false), $boolean);
    }

    public function testTrueFalse()
    {
        $boolean = (new Boolean(true))->equals(false, true);
        $this->check(false, $boolean);

        $boolean = (new Boolean(true))->equals(false);
        $this->check(new Boolean(false), $boolean);
    }

    public function testTrueTrue()
    {
        $boolean = (new Boolean(true))->equals(true, true);
        $this->check(true, $boolean);

        $boolean = (new Boolean(true))->equals(true);
        $this->check(new Boolean(true), $boolean);
    }
}
