<?php
namespace tests\Boolean;

use Boolean;
use Integer;
use tests\Test;

/**
 * class ToIntegerTest
 */
final class ToIntegerTest extends Test
{
    public function testIntegerFalse()
    {
        $this->check(0, (new Boolean(false))->toInteger(true));
        $this->check(new Integer(0), (new Boolean(false))->toInteger());
    }

    public function testIntegerTrue()
    {
        $this->check(1, (new Boolean(true))->toInteger(true));
        $this->check(new Integer(1), (new Boolean(true))->toInteger());
    }
}
