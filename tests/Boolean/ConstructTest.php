<?php
namespace tests\Boolean;

use Boolean;
use Double;
use Integer;
use Str;
use tests\Test;

/**
 * class ConstructTest
 */
final class ConstructTest extends Test
{
    public function testArguments()
    {
        $this->check(false, (new Boolean)->parse());
        $this->check(true, (new Boolean(true))->parse());
    }

    public function testBoolean()
    {
        $this->check(false, (new Boolean(false))->parse());
        $this->check(true, (new Boolean(true))->parse());

        $this->check(false, (new Boolean(new Boolean(false)))->parse());
        $this->check(true, (new Boolean(new Boolean(true)))->parse());
    }

    public function testDouble()
    {
        $this->check(false, (new Boolean(0.0))->parse());
        $this->check(true, (new Boolean(0.1))->parse());
        $this->check(true, (new Boolean(1.0))->parse());
        $this->check(true, (new Boolean(4.2))->parse());

        $this->check(false, (new Boolean(new Double(0.0)))->parse());
        $this->check(true, (new Boolean(new Double(0.1)))->parse());
        $this->check(true, (new Boolean(new Double(1.0)))->parse());
        $this->check(true, (new Boolean(new Double(4.2)))->parse());
    }

    public function testInteger()
    {
        $this->check(false, (new Boolean(0))->parse());
        $this->check(true, (new Boolean(-1))->parse());
        $this->check(true, (new Boolean(1))->parse());
        $this->check(true, (new Boolean(42))->parse());

        $this->check(false, (new Boolean(new Integer(0)))->parse());
        $this->check(true, (new Boolean(new Integer(-1)))->parse());
        $this->check(true, (new Boolean(new Integer(1)))->parse());
        $this->check(true, (new Boolean(new Integer(42)))->parse());
    }

    public function testString()
    {
        $this->check(false, (new Boolean(''))->parse());
        $this->check(true, (new Boolean('1'))->parse());
        $this->check(false, (new Boolean('0'))->parse());
        $this->check(true, (new Boolean('-1'))->parse());

        $this->check(false, (new Boolean(new Str('')))->parse());
        $this->check(true, (new Boolean(new Str('1')))->parse());
        $this->check(false, (new Boolean(new Str('0')))->parse());
        $this->check(true, (new Boolean(new Str('-1')))->parse());
    }
}
