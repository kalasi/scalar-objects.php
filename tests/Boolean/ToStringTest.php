<?php
namespace tests\Boolean;

use Boolean;
use Str;
use tests\Test;

/**
 * class ToStringTest
 */
final class ToStringTest extends Test
{
    public function testStrFalse()
    {
        $this->check('0', (new Boolean(false))->toString(true));
        $this->check(new Str('0'), (new Boolean(false))->toString());
    }

    public function testStrTrue()
    {
        $this->check('1', (new Boolean(true))->toString(true));
        $this->check(new Str('1'), (new Boolean(true))->toString());
    }
}
