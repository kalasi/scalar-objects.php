<?php
namespace tests\Boolean;

use Boolean;
use tests\Test;

/**
 * class ParseTest
 */
final class ParseTest extends Test
{
    public function testEmpty()
    {
        $this->check(false, (new Boolean)->parse());
    }

    public function testFalse()
    {
        $this->check(false, (new Boolean(false))->parse());
    }

    public function testTrue()
    {
        $this->check(true, (new Boolean(true))->parse());
    }
}
