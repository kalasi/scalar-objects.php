<?php
namespace tests\Str;

use Boolean;
use Str;
use tests\Test;

/**
 * Class IsEmptyTest
 */
final class IsEmptyTest extends Test
{
    /**
     *
     */
    public function testIntegerNegative()
    {
        $this->check(false, (new Str(42))->isEmpty(true));
        $this->check(new Boolean(false), (new Str(42))->isEmpty());

        $this->check(false, (new Str(-42))->isEmpty(true));
        $this->check(new Boolean(false), (new Str(-42))->isEmpty());
    }

    /**
     *
     */
    public function testNone()
    {
        $this->check(true, (new Str)->isEmpty(true));
        $this->check(new Boolean(true), (new Str)->isEmpty());
    }

    /**
     *
     */
    public function testStr()
    {
        $this->check(false, (new Str('Test.'))->isEmpty(true));
        $this->check(new Boolean(false), (new Str('Test.'))->isEmpty());
    }
}
