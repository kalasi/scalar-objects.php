<?php
namespace tests\Str;

use Str;
use tests\Test;

/**
 * Class UpperCaseTest
 */
final class UpperCaseTest extends Test
{
    public function testToUpperCaseActualEmpty()
    {
        $this->check('', (new Str(''))->toUpperCase(true));
        $this->check(new Str(''), (new Str(''))->toUpperCase());
    }

    public function testToUpperCaseActualValue()
    {
        $this->check('TEST.', (new Str('Test.'))->toUpperCase(true));
        $this->check(new Str('TEST.'), (new Str('Test.'))->toUpperCase());
    }
}
