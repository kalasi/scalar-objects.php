<?php
namespace tests\Str;

use Double;
use Str;
use tests\Test;

/**
 * Class ToDoubleTest
 */
final class ToDoubleTest extends Test
{
    public function testAll()
    {
        $this->check(4.0, (new Str('4.0'))->toDouble(true));
        $this->check(0.0, (new Str('0.0'))->toDouble(true));
        $this->check(-4.0, (new Str('-4.0'))->toDouble(true));

        $this->check(new Double(4.0), (new Str('4.0'))->toDouble());
        $this->check(new Double(0.0), (new Str('0.0'))->toDouble());
        $this->check(new Double(-4.0), (new Str('-4.0'))->toDouble());
    }
}
