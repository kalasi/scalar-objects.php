<?php
namespace tests\Str;

use Str;
use tests\Test;

/**
 * Class AddslashesTest
 */
final class AddslashesTest extends Test
{
    public function testEmpty()
    {
        $this->check('', (new Str)->addslashes(true));
        $this->check(new Str, (new Str)->addslashes());
    }

    public function testValueQuotesNone()
    {
        $string = (new Str('Testing.'))->addslashes(true);
        $this->check('Testing.', $string);

        $string = (new Str('Testing.'))->addslashes();
        $this->check(new Str('Testing.'), $string);
    }

    public function testValueQuotesSingle()
    {
        $string = (new Str("test aren't horrible."))->addslashes(true);
        $this->check("test aren\'t horrible.", $string);
    }

    public function testValueQuotesDouble()
    {
        $insert = '"Change is the law of life. And those who look only to the past or present are certain to miss the future." - John F. Kennedy';

        $expected = '\\"Change is the law of life. And those who look only to the past or present are certain to miss the future.\\" - John F. Kennedy';

        $string = (new Str($insert))->addslashes(true);
        $this->check($expected, $string);
    }

    public function testValueQuotesMixed()
    {
        $insert = "Here's a quote: \"Food is awesome.\" - Everyone.";
        $expected = "Here\'s a quote: \\\"Food is awesome.\\\" - Everyone.";

        $string = (new Str($insert))->addslashes(true);
        $this->check($expected, $string);
    }
}
