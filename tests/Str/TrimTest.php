<?php
namespace tests\Str;

use Str;
use tests\Test;

/**
 * Class TrimTest
 */
final class TrimTest extends Test
{
    public function testEmpty()
    {
        $this->check('', $str = (new Str(''))->trim('', true));
        $this->check(new Str(''), (new Str(''))->trim(''));
    }

    public function testNothing()
    {
        $this->check('Test.', (new Str('Test.'))->trim('', true));
        $this->check(new Str('Test.'), (new Str('Test.'))->trim(''));
    }

    public function testSpace()
    {
        $this->check('Test.', (new Str("\tTest."))->trim("\t", true));
    }
}
