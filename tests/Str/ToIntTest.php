<?php
namespace tests\Str;

use Integer;
use Str;
use tests\Test;

/**
 * Class ToIntTest
 */
final class ToIntTest extends Test
{
    public function testAll()
    {
        $this->check(4, (new Str('4'))->toInt(true));
        $this->check(0, (new Str('0'))->toInt(true));
        $this->check(-4, (new Str('-4'))->toInt(true));

        $this->check(new Integer(4), (new Str('4'))->toInt());
        $this->check(new Integer(0), (new Str('0'))->toInt());
        $this->check(new Integer(-4), (new Str('-4'))->toInt());
    }
}
