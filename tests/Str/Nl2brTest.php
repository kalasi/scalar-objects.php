<?php
namespace tests\Str;

use Str;
use tests\Test;

/**
 * Class Nl2brTest
 */
final class Nl2brTest extends Test
{
    public function testAll()
    {
        $this->check('', (new Str)->nl2br(false, true));
        $this->check("test<br>\ntest2", (new Str("test\ntest2"))->nl2br(false, true));
    }
}
