<?php
namespace tests\Str;

use Str;
use tests\Test;

/**
 * Class ToStringTest
 */
final class ToStringTest extends Test
{
    public function testAll()
    {
        $this->check(new Str('4'), (new Str('4'))->toString());
        $this->check(new Str(''), (new Str(''))->toString());
        $this->check(new Str('test'), (new Str('test'))->toString());

        $this->check('4', (new Str('4'))->toString(true));
        $this->check('', (new Str(''))->toString(true));
        $this->check('test', (new Str('test'))->toString(true));

        $this->check('4', (string) new Str('4'));
        $this->check('', (string) new Str(''));
        $this->check('test', (string) new Str('test'));
    }
}
