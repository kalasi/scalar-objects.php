<?php
namespace tests\Str;

use Str;
use tests\Test;

/**
 * Class AddcslashesTest
 */
final class AddcslashesTest extends Test
{
    public function testEmpty()
    {
        $string = (new Str)->addcslashes('A..z', true);
        $this->check('', $string);

        $string = (new Str)->addcslashes('A..z');
        $this->check(new Str, $string);
    }

    public function testValueUpperLower()
    {
        $string = (new Str('Testing.'))->addcslashes('A..z', true);
        $this->check('\T\e\s\t\i\n\g.', $string);
    }

    public function testValueQuotes()
    {
        $string = (new Str('Testing is "cool".'))->addcslashes('A..z', true);
        $this->check('\T\e\s\t\i\n\g \i\s "\c\o\o\l".', $string);
    }
}
