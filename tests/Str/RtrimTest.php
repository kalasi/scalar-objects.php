<?php
namespace tests\Str;

use Str;
use tests\Test;

/**
 * Class RtrimTest
 */
final class RtrimTest extends Test
{
    /**
     * The words to be tested in the rtrim.
     *
     * @var string
     */
    private $words = "\t\tThese are a few words :) ...";

    public function testEmpty()
    {
        $string = (new Str($this->words))->rtrim('');

        $this->check($this->words, $string->parse());
        $this->check(new Str($this->words), $string);
    }

    public function testCharacterMask()
    {
        $string = (new Str($this->words))->rtrim("\t.", true);

        $this->check("\t\tThese are a few words :) ", $string);
    }

    public function testAliasEmpty()
    {
        $string = (new Str($this->words))->chop('');

        $this->check($this->words, $string->parse());
        $this->check(new Str($this->words), $string);
    }

    public function testAliasCharacterMask()
    {
        $string = (new Str($this->words))->chop('\t.', true);

        $this->check("\t\tThese are a few words :) ", $string);
    }
}
