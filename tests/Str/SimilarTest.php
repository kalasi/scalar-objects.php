<?php
namespace tests\Str;

use Integer;
use Str;
use tests\Test;

/**
 * Class SimilarTest
 */
final class SimilarTest extends Test
{
    public function testEmpty()
    {
        $similarity = (new Str)->similar('', true);
        $this->check(0, $similarity);

        $similarity = (new Str)->similar('');
        $this->check(new Integer(0), $similarity);
    }

    public function testValue()
    {
        $similarity = (new Str('Test.'))->similar('Definitely a test.', true);
        $this->check(4, $similarity);

        $similarity = (new Str('Test.'))->similar('Definitely a test.');
        $this->check(new Integer(4), $similarity);
    }
}
