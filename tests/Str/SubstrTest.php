<?php
namespace tests\Str;

use Str;
use tests\Test;

/**
 * Class SubstrTest
 */
final class SubstrTest extends Test
{
    public function testEmpty()
    {
        $this->check('', (new Str)->substr(0, 0)->parse());
        $this->check(new Str, (new Str)->substr(0, 0));
    }

    public function testValue()
    {
        $this->check('Test', (new Str('Test.'))->substr(0, 4)->parse());
    }

    public function testReverse()
    {
        $this->check('st', (new Str('Test.'))->substr(2, -1)->parse());
    }
}
