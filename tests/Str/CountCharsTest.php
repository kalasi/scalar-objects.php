<?php
namespace tests\Str;

use Str;
use tests\Test;

/**
 * Class CountCharsTest
 */
final class CountCharsTest extends Test
{
    public function testEmptyMaskNone()
    {
        $this->check(0, (new Str)->countChars()[1]);
        $this->check(1, (new Str('test'))->countChars()[101]);
    }
}
