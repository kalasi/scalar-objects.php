<?php
namespace tests\Str;

use Double;
use Str;
use tests\Test;

/**
 * Class ToFloatTest
 */
final class ToFloatTest extends Test
{
    public function testAll()
    {
        $this->check(4.0, (new Str('4'))->toFloat(true));
        $this->check(0.0, (new Str('0'))->toFloat(true));
        $this->check(-4.0, (new Str('-4'))->toFloat(true));

        $this->check(new Double(4.0), (new Str('4'))->toFloat());
        $this->check(new Double(0.0), (new Str('0'))->toFloat());
        $this->check(new Double(-4.0), (new Str('-4'))->toFloat());
    }
}
