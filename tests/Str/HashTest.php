<?php
namespace tests\Str;

use Str;
use tests\Test;

/**
 * Class HashTest
 */
final class HashTest extends Test
{
    public function testMd5()
    {
        $string = (new Str('test'))->md5(false, true);
        $this->check('098f6bcd4621d373cade4e832627b4f6', $string);
    }

    public function testSha1()
    {
        $string = (new Str('test'))->sha1(false, true);
        $this->check('a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', $string);
    }
}
