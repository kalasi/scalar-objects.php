<?php
namespace tests\Str;

use Str;
use tests\Test;

/**
 * Class LengthTest
 */
final class LengthTest extends Test
{
    public function testLengthEmpty()
    {
        $this->check(0, (new Str)->length);
        $this->check(8, (new Str('Testing.'))->length);
    }
}
