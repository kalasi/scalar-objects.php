<?php
namespace tests\Str;

use Str;
use tests\Test;

/**
 * Class OrdTest
 */
final class OrdTest extends Test
{
    public function testAll()
    {
        $this->check(0, (new Str)->ord());
        $this->check(97, (new Str('a'))->ord());
    }
}
