<?php
namespace tests\Str;

use Str;
use tests\Test;

/**
 * Class CryptTest
 */
final class CryptTest extends Test
{
    /**
     * The length of a subStr to search for.
     *
     * @var int
     */
    private $length = 0;

    /**
     * The salt to crypt with.
     *
     * @var int
     */
    private $salt = 0;

    /**
     * The start of a subStr to search for.
     *
     * @var int
     */
    private $start = 0;

    public function __construct()
    {
        $this->length = 0;
        $this->salt = mt_rand();
        $this->start = 2;
    }

    /**
     * Check that the first 3 characters of an empty and non-empty
     * crypted Str equal the first 3 of PHP's native function.
     */
    public function testValueEmpty()
    {
        $string = substr((new Str)->crypt($this->salt, true), $this->start, $this->length);
        $this->check(substr(crypt($string, $this->salt), $this->start, $this->length), $string);

        $string = (new Str)->crypt($this->salt)->substr($this->start, $this->length);
        $this->check(new Str(substr(crypt($string, $this->salt), $this->start, $this->length)), $string);
    }

    public function testValue()
    {
        $string = substr((new Str('Test.'))->crypt($this->salt), $this->start, $this->length);
        $this->check(substr(crypt($string, $this->salt), $this->start, $this->length), $string);
    }
}
