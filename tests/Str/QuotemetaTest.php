<?php
namespace tests\Str;

use Str;
use tests\Test;

/**
 * Class QuotemetaTest
 */
final class QuotemetaTest extends Test
{
    public function testEmpty()
    {
        $this->check((string) '', (new Str)->quotemeta()->parse());
        $this->check(new Str, (new Str)->quotemeta());
    }

    public function testReplaceNone()
    {
        $this->check('test', (new Str('test'))->quotemeta()->parse());
        $this->check(new Str('test'), (new Str('test'))->quotemeta());
    }

    public function testReplace()
    {
        $this->check('test\.', (new Str('test.'))->quotemeta()->parse());
    }
}
