<?php
namespace tests\Str;

use Integer;
use Str;
use tests\Test;

/**
 * Class StrlenTest
 */
final class StrlenTest extends Test
{
    public function testStrlenEmpty()
    {
        $this->check(0, (new Str)->strlen(true));
        $this->check(new Integer(0), (new Str)->strlen());
    }

    public function testStrlenValue()
    {
        $this->check(8, (new Str('Testing.'))->strlen(true));
        $this->check(new Integer(8), (new Str('Testing.'))->strlen());
    }
}
