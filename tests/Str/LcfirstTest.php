<?php
namespace tests\Str;

use Str;
use tests\Test;

/**
 * Class LcfirstTest
 */
final class LcfirstTest extends Test
{
    public function testCaps()
    {
        $this->check('tEST.', (new Str('TEST.'))->lcfirst(true));
        $this->check(new Str('tEST.'), (new Str('TEST.'))->lcfirst());
    }

    public function testLower()
    {
        $this->check('test.', (new Str('test.'))->lcfirst(true));
        $this->check(new Str('test.'), (new Str('test.'))->lcfirst());
    }

    public function testAliasCaps()
    {
        $this->check('tEST.', (new Str('TEST.'))->lowerCaseFirst(true));

        $this->check('test.', (new Str('test.'))->lowerCaseFirst(true));
        $this->check(new Str('tEST.'), (new Str('tEST.'))->lowerCaseFirst());
    }

    public function testAliasLower()
    {
        $this->check(new Str('test.'), (new Str('test.'))->lowerCaseFirst());
    }
}
