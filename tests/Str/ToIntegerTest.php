<?php
namespace tests\Str;

use Integer;
use Str;
use tests\Test;

/**
 * Class ToIntegerTest
 */
final class ToIntegerTest extends Test
{
    public function testAll()
    {
        $this->check(4, (new Str('4'))->toInteger(true));
        $this->check(0, (new Str('0'))->toInteger(true));
        $this->check(-4, (new Str('-4'))->toInteger(true));

        $this->check(new Integer(4), (new Str('4'))->toInteger());
        $this->check(new Integer(0), (new Str('0'))->toInteger());
        $this->check(new Integer(-4), (new Str('-4'))->toInteger());
    }
}
