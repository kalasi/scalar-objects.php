<?php
namespace tests\Str;

use Str;
use tests\Test;

/**
 * Class LowerCaseTest
 */
final class LowerCaseTest extends Test
{
    public function testEmpty()
    {
        $this->check('', (new Str)->toLowerCase(true));
        $this->check(new Str(''), (new Str)->toLowerCase());
    }

    public function testValue()
    {
        $this->check('test.', (new Str('Test.'))->toLowerCase(true));
        $this->check(new Str('test.'), (new Str('Test.'))->toLowerCase());
    }
}
