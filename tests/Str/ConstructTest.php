<?php
namespace tests\Str;

use Boolean;
use Double;
use Integer;
use Str;
use tests\Test;

/**
 * test that based on the constructor's argument the Str cast returns the correct Str.
 *
 * Class ConstructTest
 */
final class ConstructTest extends Test
{
    public function testArguments()
    {
        $this->check('', (new Str)->parse());
        $this->check('', (new Str(''))->parse());
        $this->check('t', (new Str('t'))->parse());
    }

    public function testBoolean()
    {
        $this->check('', (new Str(false))->parse());
        $this->check('1', (new Str(true))->parse());
        $this->check('0', (new Str(new Boolean(false)))->parse());
        $this->check('1', (new Str(new Boolean(true)))->parse());
    }

    public function testDouble()
    {
        $this->check('9001.42', (new Str(9001.42))->parse());
        $this->check('9001.42', (new Str(new Double(9001.42)))->parse());
        $this->check('0.0', (new Str(0.0))->parse());
        $this->check('0.0', (new Str(new Double(0.0)))->parse());
        $this->check('-9001.42', (new Str(-9001.42))->parse());
        $this->check('-9001.42', (new Str(new Double(-9001.42)))->parse());
    }

    public function testInteger()
    {
        $this->check('9001', (new Str(9001))->parse());
        $this->check('9001', (new Str(new Integer(9001)))->parse());
        $this->check('0', (new Str(0))->parse());
        $this->check('0', (new Str(new Integer(0)))->parse());
        $this->check('-9001', (new Str(-9001))->parse());
        $this->check('-9001', (new Str(new Integer(-9001)))->parse());
    }

    public function testString()
    {
        $this->check('z', (new Str('z'))->parse());
        $this->check('z', (new Str(new Str('z')))->parse());
        $this->check('z', (new Str(new Str(new Str('z'))))->parse());
    }
}
