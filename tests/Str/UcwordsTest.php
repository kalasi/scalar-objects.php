<?php
namespace tests\Str;

use Str;
use tests\Test;

/**
 * Class UcwordsTest
 */
final class UcwordsTest extends Test
{
    public function testLowerUcwords()
    {
        $str = (new Str('TESTING THIS.'))->toLowerCase()->ucwords(true);
        $this->check('Testing This.', $str);

        $str = (new Str('TESTING THIS.'))->toLowerCase()->ucwords();
        $this->check(new Str('Testing This.'), $str);
    }

    public function testUcwordsEmpty()
    {
        $str = (new Str(''))->ucwords(true);
        $this->check('', $str);

        $str = (new Str(''))->ucwords();
        $this->check(new Str(''), $str);
    }

    public function testUcwordsLower()
    {
        $str = (new Str('testing this.'))->ucwords(true);
        $this->check('Testing This.', $str);

        $str = (new Str('testing this.'))->ucwords();
        $this->check(new Str('Testing This.'), $str);
    }

    public function testUcwordsMixed()
    {
        $str = (new Str('TEsTIng thIs.'))->ucwords(true);
        $this->check('TEsTIng ThIs.', $str);

        $str = (new Str('TEsTIng thIs.'))->ucwords();
        $this->check(new Str('TEsTIng ThIs.'), $str);
    }

    public function testUcwordsUpper()
    {
        $str = (new Str('TESTING THIS.'))->ucwords(true);
        $this->check('TESTING THIS.', $str);

        $str = (new Str('TESTING THIS.'))->ucwords();
        $this->check(new Str('TESTING THIS.'), $str);
    }
}
