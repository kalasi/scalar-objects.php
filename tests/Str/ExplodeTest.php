<?php
namespace tests\Str;

use Str;
use tests\Test;

/**
 * Class ExplodeTest
 */
final class ExplodeTest extends Test
{
    /**
     * The delimiter to explode by.
     *
     * @var Str
     */
    private $delimiter = '';

    public function __construct()
    {
        $this->delimiter = '~';
    }

    public function testBasic()
    {
        $array = (new Str('2~3'))->explode($this->delimiter);
        $this->check(['2', '3'], $array);
    }

    public function testLeft()
    {
        $array = (new Str('2~'))->explode($this->delimiter);
        $this->check(['2', ''], $array);
    }

    public function testRight()
    {
        $array = (new Str('~4'))->explode($this->delimiter);
        $this->check(['', '4'], $array);
    }

    public function testNone()
    {
        $array = (new Str('~'))->explode($this->delimiter);
        $this->check(['', ''], $array);
    }

    public function testMultiple()
    {
        $array = (new Str('2~3~4'))->explode($this->delimiter);
        $this->check(['2', '3', '4'], $array);
    }
}
