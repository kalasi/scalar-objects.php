<?php
namespace tests\Double;

use Double;
use ScalarObjects\Exceptions\DivideByZero;
use tests\Test;

/**
 * class DivideTest
 */
final class DivideTest extends Test
{
    public function testDivideByNegative()
    {
        $integer = (new Double(12.0))->divideBy(-3, true);
        $this->check(-4.0, $integer);

        $integer = (new Double(12.0))->divideBy(-3);
        $this->check(new Double(-4.0), $integer);
    }

    public function testDivideByPositive()
    {
        $integer = (new Double(12.0))->divideBy(3, true);
        $this->check(4.0, $integer);

        $integer = (new Double(12.0))->divideBy(3);
        $this->check(new Double(4.0), $integer);
    }

    public function testDivideByZero()
    {
        $integer = (new Double(12.0));

        $exception = '';

        try {
            $integer->divideBy(0, true);
        } catch (DivideByZero $e) {
            $exception = 'caught';
        }

        $this->check('caught', $exception);

        $integer = (new Double(12.0));

        $exception = '';

        try {
            $integer->divideBy(0);
        } catch (DivideByZero $e) {
            $exception = 'caught';
        }

        $this->check('caught', $exception);
    }

    public function testDivideFromNegative()
    {
        $integer = (new Double(3.0))->divideFrom(-12, true);
        $this->check(-4.0, $integer);

        $integer = (new Double(3.0))->divideFrom(-12);
        $this->check(new Double(-4.0), $integer);
    }

    public function testDivideFromPositive()
    {
        $integer = (new Double(3.0))->divideFrom(12, true);
        $this->check(4.0, $integer);

        $integer = (new Double(3.0))->divideFrom(12);
        $this->check(new Double(4.0), $integer);
    }

    public function testDivideFromZero()
    {
        $integer = (new Double(12.0))->divideFrom(0, true);
        $this->check(0.0, $integer);

        $integer = (new Double(12.0))->divideFrom(0);
        $this->check(new Double(0.0), $integer);
    }

    public function testDivideNegative()
    {
        $integer = (new Double(12.0))->divide(-3, true);
        $this->check(-4.0, $integer);

        $integer = (new Double(12.0))->divide(-3);
        $this->check(new Double(-4.0), $integer);
    }

    public function testDividePositive()
    {
        $integer = (new Double(12.0))->divide(3, true);
        $this->check(4.0, $integer);

        $integer = (new Double(12.0))->divide(3);

        $this->check(new Double(4.0), $integer);
    }

    public function testDivideZero()
    {
        $integer = (new Double(12.0));

        $exception = '';

        try {
            $integer->divide(0, true);
        } catch (DivideByZero $e) {
            $exception = 'caught';
        }

        $this->check('caught', $exception);

        $integer = (new Double(12.0));

        $exception = '';

        try {
            $integer->divide(0);
        } catch (DivideByZero $e) {
            $exception = 'caught';
        }

        $this->check('caught', $exception);
    }
}
