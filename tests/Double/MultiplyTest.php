<?php
namespace tests\Double;

use Double;
use tests\Test;

/**
 * class MultiplyTest
 */
final class MultiplyTest extends Test
{
    public function testMultiplyNegative()
    {
        $integer = (new Double(4.0))->multiply(-4, true);
        $this->check(-16.0, $integer);

        $integer = (new Double(4.0))->multiply(-4);
        $this->check(new Double(-16.0), $integer);
    }

    public function testMultiplyPositive()
    {
        $integer = (new Double(4.0))->multiply(4, true);
        $this->check(16.0, $integer);

        $integer = (new Double(4.0))->multiply(4);
        $this->check(new Double(16.0), $integer);
    }

    public function testMultiplyZero()
    {
        $integer = (new Double(4.0))->multiply(0, true);
        $this->check(0.0, $integer);

        $integer = (new Double(4.0))->multiply(0);
        $this->check(new Double(0.0), $integer);
    }
}
