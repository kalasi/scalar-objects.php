<?php
namespace tests\Double;

use Double;
use tests\Test;

/**
 * Class ToStringTest
 */
final class ToStringTest extends Test
{
    public function testPrimitive()
    {
        $this->check('0', (new Double)->toString(true));
        $this->check('4', (new Double(4.0))->toString(true));
        $this->check('0', (new Double(0.0))->toString(true));
        $this->check('-4', (new Double(-4.0))->toString(true));
    }
}
