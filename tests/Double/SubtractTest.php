<?php
namespace tests\Double;

use Double;
use tests\Test;

/**
 * class SubtractTest
 */
final class SubtractTest extends Test
{
    public function testSubtractNegative()
    {
        $integer = (new Double(4.0))->subtract(-2, true);
        $this->check(6.0, $integer);

        $integer = (new Double(4.0))->subtract(-2);
        $this->check(new Double(6.0), $integer);
    }

    public function testSubtractPositive()
    {
        $integer = (new Double(4.0))->subtract(2, true);
        $this->check(2.0, $integer);

        $integer = (new Double(4.0))->subtract(2);
        $this->check(new Double(2.0), $integer);
    }

    public function testSubtractZero()
    {
        $integer = (new Double(4.0))->subtract(0, true);
        $this->check(4.0, $integer);

        $integer = (new Double(4.0))->subtract(0);
        $this->check(new Double(4.0), $integer);
    }

    public function testSubtractByNegative()
    {
        $integer = (new Double(4.0))->subtractBy(-2, true);
        $this->check(6.0, $integer);

        $integer = (new Double(4.0))->subtractBy(-2);
        $this->check(new Double(6.0), $integer);
    }

    public function testSubtractByPositive()
    {
        $integer = (new Double(4.0))->subtractBy(2, true);
        $this->check(2.0, $integer);

        $integer = (new Double(4.0))->subtractBy(2);
        $this->check(new Double(2.0), $integer);
    }

    public function testSubtractByZero()
    {
        $integer = (new Double(4.0))->subtractBy(0, true);
        $this->check(4.0, $integer);

        $integer = (new Double(4.0))->subtractBy(0);
        $this->check(new Double(4.0), $integer);
    }
}
