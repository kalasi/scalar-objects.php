<?php
namespace tests\Double;

use Double;
use Integer;
use tests\Test;

/**
 * Class ToIntegerTest
 */
final class ToIntegerTest extends Test
{
    public function testAll()
    {
        $this->check(0, (new Double)->toInteger(true));
        $this->check(4, (new Double(4.0))->toInteger(true));
        $this->check(0, (new Double(0.0))->toInteger(true));
        $this->check(-4, (new Double(-4.0))->toInteger(true));
        $this->check(1, (new Double(1.1))->toInteger(true));

        $this->check(new Integer(0), (new Double)->toInteger());
        $this->check(new Integer(4), (new Double(4.0))->toInteger());
        $this->check(new Integer(0), (new Double(0.0))->toInteger());
        $this->check(new Integer(-4), (new Double(-4.0))->toInteger());
        $this->check(new Integer(1), (new Double(1.1))->toInteger());
    }
}
