<?php
namespace tests\Double;

use Double;
use Integer;
use tests\Test;

/**
 * Class ToIntTest
 */
final class ToIntTest extends Test
{
    public function testAll()
    {
        $this->check(0, (new Double)->toInt(true));
        $this->check(4, (new Double(4.0))->toInt(true));
        $this->check(0, (new Double(0.0))->toInt(true));
        $this->check(-4, (new Double(-4.0))->toInt(true));
        $this->check(1, (new Double(1.1))->toInt(true));

        $this->check(new Integer(0), (new Double)->toInt());
        $this->check(new Integer(4), (new Double(4.0))->toInt());
        $this->check(new Integer(0), (new Double(0.0))->toInt());
        $this->check(new Integer(-4), (new Double(-4.0))->toInt());
        $this->check(new Integer(1), (new Double(1.1))->toInt());
    }
}
