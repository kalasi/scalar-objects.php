<?php
namespace tests\Double;

use Double;
use Integer;
use tests\Test;

/**
 * Class ConstructTest
 */
final class ConstructTest extends Test
{
    public function testArguments()
    {
        $this->check(0.0, (new Double)->parse());
        $this->check(1.1, (new Double(1.1))->parse());
    }

    public function testDouble()
    {
        $this->check(1.1, (new Double(1.1))->parse());
        $this->check(0.0, (new Double(0))->parse());
        $this->check(-1.1, (new Double(-1.1))->parse());
        $this->check(1.1, (new Double(new Double(1.1)))->parse());
        $this->check(0.0, (new Double(new Double))->parse());
        $this->check(-1.1, (new Double(new Double(-1.1)))->parse());
    }

    public function testInteger()
    {
        $this->check(1.0, (new Double(1))->parse());
        $this->check(0.0, (new Double(0))->parse());
        $this->check(-1.0, (new Double(-1))->parse());
        $this->check(1.0, (new Double(new Integer(1)))->parse());
        $this->check(0.0, (new Double(new Integer(0)))->parse());
        $this->check(-1.0, (new Double(new Integer(-1)))->parse());
    }

    public function testString()
    {
        $this->check(1.1, (new Double('1.1'))->parse());
        $this->check(0.0, (new Double(''))->parse());
        $this->check(-1.1, (new Double('-1.1'))->parse());
        $this->check(1.1, (new Double(new Double(1.1)))->parse());
        $this->check(0.0, (new Double(new Double))->parse());
        $this->check(-1.1, (new Double(new Double(-1.1)))->parse());
    }
}
