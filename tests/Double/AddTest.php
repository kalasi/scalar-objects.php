<?php
namespace tests\Double;

use Double;
use tests\Test;

/**
 * class AddTest
 */
final class AddTest extends Test
{
    public function testAddByNegative()
    {
        $integer = (new Double(42.0))->addBy(-13);
        $this->check(new Double(29.0), $integer);

        $integer = (new Double(42.0))->addBy(-13, true);
        $this->check(29.0, $integer);
    }

    public function testAddByPositive()
    {
        $integer = (new Double(42.0))->addBy(13);
        $this->check(new Double(55.0), $integer);

        $integer = (new Double(42.0))->addBy(13, true);
        $this->check(55.0, $integer);
    }

    public function testAddByZero()
    {
        $integer = (new Double(42.0))->addBy(0);
        $this->check(new Double(42.0), $integer);

        $integer = (new Double(42.0))->addBy(0, true);
        $this->check(42.0, $integer);
    }

    public function testAddNegative()
    {
        $integer = (new Double(42.0))->add(-13);
        $this->check(new Double(29.0), $integer);

        $integer = (new Double(42.0))->add(-13, true);
        $this->check(29.0, $integer);
    }

    public function testAddPositive()
    {
        $integer = (new Double(42.0))->add(13);
        $this->check(new Double(55.0), $integer);

        $integer = (new Double(42.0))->add(13, true);
        $this->check(55.0, $integer);
    }

    public function testAddZero()
    {
        $integer = (new Double(42.0))->add(0);
        $this->check(new Double(42.0), $integer);

        $integer = (new Double(42.0))->add(0, true);
        $this->check(42.0, $integer);
    }
}
