<?php
namespace tests;

use PHPUnit_Framework_TestCase;
use ReflectionObject;

/**
 * The base class that tests extend from.
 *
 * Class Test
 */
abstract class Test extends PHPUnit_Framework_TestCase
{
    /**
     * Called on the class construct.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Asserts that two given classes are both equal and that their properties are equal.
     *
     * @param $class1
     * @param $class2
     *
     * @return bool
     */
    public function check($class1, $class2)
    {
        $property1 = $class1;
        $property2 = $class2;

        $bothAreObjects = is_object($class1) === is_object($class2);

        if (!$bothAreObjects) {
            die('One of the provided are not objects.');
        }

        if (is_object($class1)) {
            $property1 = (new ReflectionObject($class1))->getProperty('val');
        }

        if (is_object($class2)) {
            $property2 = (new ReflectionObject($class2))->getProperty('val');
        }

        if (gettype($class1) !== gettype($class2)) {
            die('The provided variables are not of the same type.');
        }

        $propertiesSame = $property1 === $property2;
        $classesSame = $class1 === $class2;

        $this->assertEquals($property1, $property2);
        $this->assertEquals($class1, $class2);
    }
}
