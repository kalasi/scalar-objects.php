<?php

use ScalarObjects\ObjectInterface;

/**
 * Class Str
 */
class Str implements ObjectInterface
{
    /**
     * The length of $val.
     *
     * @var int
     */
    public $length = 0;

    /**
     * The value of the scalar object.  Set by the constructor.
     *
     * @var string
     */
    private $val = '';

    /**
     * Sets $val to the string given in the constructor.  If none is given, then set it to ''.
     *
     * @param string $val
     */
    public function __construct($val = '')
    {
        $this->val = (string) $val;

        $this->length = strlen($this->val);
    }

    /**
     * Adds slashes to quotes in a C style.
     *
     * @param      $charlist
     * @param bool $primitive
     *
     * @return $this|string
     */
    public function addcslashes($charlist, $primitive = false)
    {
        $result = addcslashes($this->val, $charlist);

        if ($primitive) {
            return $result;
        }

        $this->val = $result;

        return $this;
    }

    /**
     * Adds slashes to quotes.
     *
     * @param bool $primitive
     *
     * @return $this|string
     */
    public function addslashes($primitive = false)
    {
        $result = addslashes($this->val);

        if ($primitive) {
            return $result;
        }

        $this->val = $result;

        return $this;
    }

    /**
     * Alias of rtrim.
     *
     * @param      $characterMask
     * @param bool $primitive
     *
     * @return $this|string
     */
    public function chop($characterMask, $primitive = false)
    {
        return $this->rtrim($characterMask, $primitive);
    }

    /**
     * Returns an array of string (depending on mode) of the chars in the string.
     *
     * @param int $mode
     *
     * @return mixed
     */
    public function countChars($mode = 0)
    {
        return count_chars($this->val, $mode);
    }

    /**
     * Returns a hashed string object using the DES-based algorithm.
     *
     * @param string $salt
     * @param bool   $primitive
     *
     * @return $this|string
     */
    public function crypt($salt = '', $primitive = false)
    {
        $result = crypt($this->val, $salt);

        if ($primitive) {
            return $result;
        }

        $this->val = $result;

        return $this;
    }

    /**
     * Returns an array of this object being exploded by the given delimiter.
     *
     * @param $delimiter
     *
     * @return array
     */
    public function explode($delimiter)
    {
        return explode($delimiter, $this->val);
    }

    /**
     * @param null   $flags
     * @param string $encoding
     *
     * @return $this
     */
    public function htmlEntityDecode($flags = null, $encoding = '')
    {
        if ($flags === null) {
            $flags = ENT_COMPAT | ENT_HTML401;
        }

        if ($encoding === '') {
            $encoding = ini_get('default_charset');
        }

        $this->val = html_entity_decode($this->val, $flags, $encoding);

        return $this;
    }

    /**
     * @param null   $flags
     * @param string $encoding
     * @param bool   $doubleEncode
     *
     * @return $this
     */
    public function htmlEntities($flags = null, $encoding = '', $doubleEncode = true)
    {
        if ($flags === null) {
            $flags = ENT_COMPAT | ENT_HTML401;
        }

        if ($encoding === '') {
            $encoding = ini_get('default_charset');
        }

        $this->val = htmlentities($this->val, $flags, $encoding, $doubleEncode);

        return $this;
    }

    /**
     * @param null $flags
     *
     * @return $this
     */
    public function htmlSpecialCharsDecode($flags = null)
    {
        if ($flags === null) {
            $flags = ENT_COMPAT | ENT_HTML401;
        }

        $this->val = htmlspecialchars_decode($this->val, $flags);

        return $this;
    }

    /**
     * @param null   $flags
     * @param string $encoding
     * @param bool   $doubleEncode
     *
     * @return $this
     */
    public function htmlSpecialChars($flags = null, $encoding = '', $doubleEncode = true)
    {
        if ($flags === null) {
            $flags = ENT_COMPAT | ENT_HTML401;
        }

        if ($encoding === '') {
            $encoding = ini_get('default_charset');
        }

        $this->val = htmlspecialchars($this->val, $flags, $encoding, $doubleEncode);

        return $this;
    }

    /**
     * Returns a boolean of whether or not $val is empty.
     * If $val is '', return true. If not, return false.
     *
     * @param bool $primitive
     *
     * @return bool|Boolean
     */
    public function isEmpty($primitive = false)
    {
        $result = strlen($this->val) === 0;

        if ($primitive) {
            return $result;
        }

        return new Boolean($result);
    }

    /**
     * Lower cases the first character in the string.
     *
     * @param bool $primitive
     *
     * @return $this|string
     */
    public function lcfirst($primitive = false)
    {
        $result = lcfirst($this->val);

        if ($primitive) {
            return $result;
        }

        $this->val = $result;

        return $this;
    }

    /**
     * Calculates the levenshtein distance between this string and another, with added difficulty if requested.
     *
     * @param string $compare
     * @param int    $costInsertion
     * @param int    $costReplacement
     * @param int    $costDeletion
     *
     * @return int
     */
    public function levenshtein($compare, $costInsertion = 0, $costReplacement = 0, $costDeletion = 0)
    {
        if ($costInsertion > 0 && $costReplacement > 0 && $costDeletion > 0) {
            return levenshtein($this->val, $compare, $costInsertion, $costReplacement, $costDeletion);
        }

        return levenshtein($this->val, $compare);
    }

    /**
     * Alias for lcfirst.
     *
     * @param bool $primitive
     *
     * @return $this|string
     */
    public function lowerCaseFirst($primitive = false)
    {
        return $this->lcfirst($primitive);
    }

    /**
     * Trims whitespace from the beginning of the string.
     *
     * @param string $characterMask
     * @param bool   $primitive
     *
     * @return $this
     */
    public function ltrim($characterMask = '', $primitive = false)
    {
        $result = ltrim($characterMask);

        if ($primitive) {
            return $result;
        }

        $this->val = $result;

        return $this;
    }

    /**
     * Converts the string into an md5 hash.
     *
     * @param bool $rawOutput
     * @param bool $primitive
     *
     * @return $this|string
     */
    public function md5($rawOutput = false, $primitive = false)
    {
        $result = md5($this->val, $rawOutput);

        if ($primitive) {
            return $result;
        }

        $this->val = $result;

        return $this;
    }

    /**
     * Converts newlines into breaks.
     *
     * @param bool $isXhtml
     * @param bool $primitive
     *
     * @return $this|string
     */
    public function nl2br($isXhtml = false, $primitive = false)
    {
        $result = nl2br($this->val, $isXhtml);

        if ($primitive) {
            return $result;
        }

        $this->val = $result;

        return $this;
    }

    /**
     * Returns the ASCII value of a character.
     *
     * @return int
     */
    public function ord()
    {
        return ord($this->val);
    }

    /**
     * Returns this as a primitive string.
     *
     * @return string
     */
    public function parse()
    {
        return (string) $this->val;
    }

    /**
     * Quotes meta characters in the string.
     *
     * @param bool $primitive
     *
     * @return $this|string
     */
    public function quotemeta($primitive = false)
    {
        $result = quotemeta($this->val);

        if ($primitive) {
            return $result;
        }

        $this->val = $result;

        return $this;
    }

    /**
     * Calculates a sha1 hash of this string.
     *
     * @param bool $rawOutput
     * @param bool $primitive
     *
     * @return $this|string
     */
    public function sha1($rawOutput = false, $primitive = false)
    {
        $result = sha1($this->val, $rawOutput);

        if ($primitive) {
            return $result;
        }

        $this->val = $result;

        return $this;
    }

    /**
     * Compares the similarity between this string and a passed string.
     * Pass a variable by reference to return a given percentage.
     *
     * @param string $compare
     * @param bool   $primitive
     *
     * @return int|Integer
     */
    public function similar($compare, $primitive = false)
    {
        $result = similar_text($this->val, $compare);

        if ($primitive) {
            return $result;
        }

        return new Integer($result);
    }

    /**
     * Strips whitespace from the end of the string.
     *
     * @param string $characterMask
     * @param bool   $primitive
     *
     * @return $this|string
     */
    public function rtrim($characterMask = '', $primitive = false)
    {
        $result = rtrim($this->val, $characterMask);

        if ($primitive) {
            return $result;
        }

        $this->val = $result;

        return $this;
    }

    /**
     * Returns the length of the string.
     *
     * @param bool $primitive
     *
     * @return int|Integer
     */
    public function strlen($primitive = false)
    {
        $result = strlen($this->val);

        if ($primitive) {
            return $result;
        }

        return new Integer($result);
    }

    /**
     * Sets a substring from this scalar object given from the given point to the given point.
     *
     * @param int  $start
     * @param int  $length
     * @param bool $primitive
     *
     * @return $this|string
     */
    public function substr($start, $length = 0, $primitive = false)
    {
        $result = substr($this->val, $start, $length);

        if ($primitive) {
            return $result;
        }

        $this->val = $result;

        return $this;
    }

    /**
     * Returns this as a primitive double or Double.
     *
     * @param bool $primitive
     *
     * @return double|Double
     */
    public function toDouble($primitive = false)
    {
        if ($primitive) {
            return (double) $this->val;
        }

        return new Double($this->val);
    }

    /**
     * Alias for toDouble.
     *
     * @param bool $primitive
     *
     * @return double|Double
     */
    public function toFloat($primitive = false)
    {
        return $this->toDouble($primitive);
    }

    /**
     * Returns this as a primitive int or Integer.
     *
     * @param bool $primitive
     *
     * @return int|Integer
     */
    public function toInteger($primitive = false)
    {
        if ($primitive) {
            return (int) $this->val;
        }

        return new Integer($this->val);
    }

    /**
     * Alias for toInteger.
     *
     * @param bool $primitive
     *
     * @return int|Integer
     */
    public function toInt($primitive = false)
    {
        return $this->toInteger($primitive);
    }

    /**
     * Returns the string in lower case.
     *
     * @param bool $primitive
     *
     * @return $this|string
     */
    public function toLowerCase($primitive = false)
    {
        $result = strtolower($this->val);

        if ($primitive) {
            return $result;
        }

        $this->val = $result;

        return $this;
    }

    /**
     * Returns this as a primitive string or String.
     *
     * @param bool $primitive
     *
     * @return string|String
     */
    public function toString($primitive = false)
    {
        if ($primitive) {
            return (string) $this->val;
        }

        return new Str($this->val);
    }

    /**
     * Returns the string in upper case.
     *
     * @param bool $primitive
     *
     * @return $this|string
     */
    public function toUpperCase($primitive = false)
    {
        $result = strtoupper($this->val);

        if ($primitive) {
            return $result;
        }

        $this->val = $result;

        return $this;
    }

    /**
     * Trims whitespace such as spaces and tabs from $val.
     *
     * @param string $characterMask
     * @param bool   $primitive
     *
     * @return $this|string
     */
    public function trim($characterMask = '', $primitive = false)
    {
        $result = trim($this->val, $characterMask);

        if ($primitive) {
            return $result;
        }

        $this->val = $result;

        return $this;
    }

    /**
     * Capitalizes all of the words in the string.
     *
     * @param bool $primitive
     *
     * @return $this|string
     */
    public function ucwords($primitive = false)
    {
        $result = ucwords($this->val);

        if ($primitive) {
            return $result;
        }

        $this->val = $result;

        return $this;
    }

    /**
     * Sets the property being set and resets the length of $val.
     *
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        if ($value == 'val') {
            $this->length = strlen($value);
        }

        $this->{$name} = $value;
    }

    /**
     * Returns $val when cast to a string.
     *
     * Examples of usage:
     *   echo $stringObject;
     *   (string) $stringObject;
     *
     * @return mixed
     */
    public function __toString()
    {
        return $this->toString(true);
    }
}
