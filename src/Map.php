<?php

use ScalarObjects\ObjectInterface;

/**
 * This class is based off of Hack's collection Map.
 *
 * Class Map
 */
class Map implements ObjectInterface
{
    /**
     * @var array
     */
    private $val = [];

    /**
     * @param array $map
     */
    public function __construct(array $map = [])
    {
        if (count($map) > 0) {
            $valid = true;
            foreach ($map as $key => $index) {
                if (!is_int($key) && !is_string($key)) {
                    $valid = false;

                    break;
                }
            }

            if (!$valid) {
                $map = [];
            }
        }

        $this->val = $map;
    }

    /**
     * Returns this as a primitive array.
     *
     * @return array
     */
    public function parse()
    {
        return (array) $this->val;
    }

    /**
     * Alias for parse.
     *
     * @return array
     */
    public function toArray()
    {
        return $this->parse();
    }

    /**
     * Returns this as a primitive string or String.
     *
     * @param bool $primitive
     *
     * @return String
     */
    public function toString($primitive = false)
    {
        if ($primitive) {
            return (string) $this->val;
        }

        return new Str($this->val);
    }

    /**
     * Returns $val when cast to a string.
     *
     * Examples of usage:
     *   echo $intObject;
     *   (string) $intObject;
     *
     * @return string
     */
    public function __toString()
    {
        return $this->toString(true);
    }
}
