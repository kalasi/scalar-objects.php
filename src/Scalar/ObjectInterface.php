<?php
namespace ScalarObjects;

/**
 * Interface for scalar objects. All of them should implement at least these methods in some form or another.
 *
 * Interface ObjectInterface
 */
interface ObjectInterface
{
    /**
     * Called on the construct of a scalar object.  An argument can optionally be passed.
     */
    public function __construct();

    /**
     * Returns the scalar object as its primitive form.  An Integer becomes an int, a String becomes a string, etc.
     *
     * @return mixed
     */
    public function parse();

    /**
     * Returns this as a primitive string or String.
     *
     * @param bool $primitive
     *
     * @return string|String
     */
    public function toString($primitive = false);

    /**
     * Returns $val when cast to a string.
     *
     * Examples of usage:
     *   echo $intObject;
     *   (string) $intObject;
     *
     * @return string
     */
    public function __toString();
}
