<?php
namespace ScalarObjects;

use Double;
use Integer;
use ScalarObjects\Exceptions\DivideByZero;
use ScalarObjects\Exceptions\InvalidType;

/**
 * Defines some arithmatical methods for an object. At the moment this should be implemented
 * by the Double and Integer objects. This is basic stuff like addition, division, etc.
 *
 * Class Arithmatic
 */
trait Arithmatic
{
    /**
     * Adds a number to this.
     *
     * @param double|Double|int|Integer $number
     * @param bool                      $primitive
     *
     * @throws InvalidType
     *
     * @return $this|double|Double|int|Integer
     */
    public function add($number, $primitive = false)
    {
        try {
            $number = $this->checkNumber($number);
        } catch (InvalidType $e) {
            throw $e;
        }

        $result = $this->val + $number;

        if ($primitive) {
            return $result;
        }

        if (is_double($result)) {
            return new Double($result);
        }

        return new Integer($result);
    }

    /**
     * Alias for add.
     *
     * @param      $number
     * @param bool $primitive
     *
     * @return $this|double|Double|int|Integer
     */
    public function addBy($number, $primitive = false)
    {
        return $this->add($number, $primitive);
    }

    /**
     * Checks to see if a given $number is a double, Double, int, or Integer.
     *
     * @param $number double|Double|int|Integer
     *
     * @throws InvalidType
     *
     * @return double|int
     */
    public function checkNumber($number)
    {
        if (is_numeric($number)) {
            if (is_double($number)) {
                return (double) $number;
            } elseif ($number instanceof Double) {
                return (double) $number->parse();
            } elseif (is_int($number)) {
                return (int) $number;
            } elseif ($number instanceof Integer) {
                return (int) $number->parse();
            }
        }

        throw new InvalidType;
    }

    /**
     * Alias for divideBy.
     *
     * @param      $number
     * @param bool $primitive
     *
     * @return $this|double
     */
    public function divide($number, $primitive = false)
    {
        return $this->divideBy($number, $primitive);
    }

    /**
     * Divides this by a given $number.
     *
     * @param      $number
     * @param bool $primitive
     *
     * @throws DivideByZero
     * @throws InvalidType
     * @throws \Exception
     *
     * @return $this|double|Double|int|Integer
     */
    public function divideBy($number, $primitive = false)
    {
        try {
            $number = $this->checkNumber($number);
        } catch (InvalidType $e) {
            throw $e;
        }

        if ($number === 0) {
            throw new DivideByZero;
        }

        $result = $this->val / $number;

        if ($primitive) {
            return $result;
        }

        if (is_double($result)) {
            return new Double($result);
        }

        return new Integer($result);
    }

    /**
     * Divides this from a given $number.
     *
     * @param      $number
     * @param bool $primitive
     *
     * @throws DivideByZero
     * @throws InvalidType
     * @throws \Exception
     *
     * @return $this|double|Double|int|Integer
     */
    public function divideFrom($number, $primitive = false)
    {
        try {
            $number = $this->checkNumber($number);
        } catch (InvalidType $e) {
            throw $e;
        }

        if ($this->val === 0) {
            throw new DivideByZero;
        }

        $result = $number / $this->val;

        if ($primitive) {
            return $result;
        }

        if (is_double($result)) {
            return new Double($result);
        }

        return new Integer($result);
    }

    /**
     * Alias for multiplyBy.
     *
     * @param      $number
     * @param bool $primitive
     *
     * @return $this|double|Double|int|Integer
     */
    public function multiply($number, $primitive = false)
    {
        return $this->multiplyBy($number, $primitive);
    }

    /**
     * Multiplies this by a number.
     *
     * @param      $number
     * @param bool $primitive
     *
     * @throws InvalidType
     * @throws \Exception
     *
     * @return $this|double|Double|int|Integer
     */
    public function multiplyBy($number, $primitive = false)
    {
        try {
            $number = $this->checkNumber($number);
        } catch (InvalidType $e) {
            throw $e;
        }

        $result = $this->val * $number;

        if ($primitive) {
            return $result;
        }

        if (is_double($result)) {
            return new Double($result);
        }

        return new Integer($result);
    }

    /**
     * Subtracts this by a number.
     *
     * @param      $number
     * @param bool $primitive
     *
     * @throws InvalidType
     * @throws \Exception
     *
     * @return $this|double|Double|int|Integer
     */
    public function subtract($number, $primitive = false)
    {
        try {
            $number = $this->checkNumber($number);
        } catch (InvalidType $e) {
            throw $e;
        }

        $result = $this->val - $number;

        if ($primitive) {
            return $result;
        }

        if (is_double($result)) {
            return new Double($result);
        }

        return new Integer($result);
    }

    /**
     * Alias for subtract.
     *
     * @param $number
     * @param $primitive
     *
     * @return $this|double|Double|int|Integer
     */
    public function subtractBy($number, $primitive = false)
    {
        return $this->subtract($number, $primitive);
    }

    /**
     * Subtracts this from a number.
     *
     * @param      $number
     * @param bool $primitive
     *
     * @throws InvalidType
     * @throws \Exception
     *
     * @return $this|double|Double|int|Integer
     */
    public function subtractFrom($number, $primitive = false)
    {
        try {
            $number = $this->checkNumber($number);
        } catch (InvalidType $e) {
            throw $e;
        }

        $result = $number - $this->val;

        if ($primitive) {
            return $result;
        }

        if (is_double($result)) {
            return new Double($result);
        }

        return new Integer($result);
    }
}
