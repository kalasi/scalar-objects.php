<?php
namespace ScalarObjects\Exceptions;

/**
 * Class InvalidType
 */
class InvalidType extends ScalarException
{
    public $message = 'The argument given must be a double, Double, int, or Integer.';
}
