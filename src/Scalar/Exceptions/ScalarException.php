<?php
namespace ScalarObjects\Exceptions;

use Exception;

/**
 * Class Exception
 */
abstract class ScalarException extends Exception implements ExceptionInterface
{
    /**
     * Exception message.
     *
     * @var string
     */
    protected $message = 'Unknown exception';

    /**
     * User-defined exception code.
     *
     * @var int
     */
    protected $code = 0;

    /**
     * Source filename of exception.
     *
     * @var
     */
    protected $file;

    /**
     * Source line of exception.
     *
     * @var
     */
    protected $line;

    /**
     * On construct.
     *
     * @param null $message
     * @param int  $code
     */
    public function __construct($message = null, $code = 0)
    {
        if (!$message) {
            throw new $this('Unknown ' . get_class($this));
        }

        parent::__construct($message, $code);
    }

    /**
     * Called when cast as a string.
     *
     * @return string
     */
    public function __toString()
    {
        $message = get_class($this) . " '" . $this->message . "' in " . $this->file;
        $message .= '(' . $this->line . ")\n" . $this->getTraceAsString();

        return $message;
    }
}
