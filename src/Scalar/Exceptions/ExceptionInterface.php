<?php
namespace ScalarObjects\Exceptions;

/**
 * Interface for ScalarException.
 *
 * Interface ExceptionInterface
 */
interface ExceptionInterface
{
    /**
     * Exception message.
     *
     * @return mixed
     */
    public function getMessage();

    /**
     * User-defined Exception code.
     *
     * @return mixed
     */
    public function getCode();

    /**
     * Source filename.
     *
     * @return mixed
     */
    public function getFile();

    /**
     * Source line number.
     *
     * @return mixed
     */
    public function getLine();

    /**
     * An array of the backtrace().
     *
     * @return mixed
     */
    public function getTrace();

    /**
     * Formatted string of trace.
     *
     * @return mixed
     */
    public function getTraceAsString();

    /**
     * Formatted string for display.
     *
     * @return mixed
     */
    public function __toString();

    /**
     * On construct.
     *
     * @param null $message
     * @param int  $code
     */
    public function __construct($message = null, $code = 0);
}
