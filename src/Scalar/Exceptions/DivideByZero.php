<?php
namespace ScalarObjects\Exceptions;

/**
 * Used specifically by the trait Arithmatic when dividing by zero.  Since mathematically that is... complicated,
 * this is thrown.
 *
 * Class DivideByZero
 */
class DivideByZero extends ScalarException
{
    public $message = 'Division by zero not allowed.';
}
