<?php

use ScalarObjects\ObjectInterface;

/**
 * This class is based off of Hack's collection Set.
 *
 * Class Set
 */
class Set implements ObjectInterface
{
    /**
     * @var array
     */
    private $val = [];

    /**
     * @param array $set
     */
    public function __construct(array $set = [])
    {
        $list = [];

        if (count($set) > 0) {
            $valid = true;

            foreach ($set as $value) {
                if (is_int($value) || is_string($value)) {
                    $list[] = $value;
                } elseif ($value instanceof Integer || $value instanceof Str) {
                    $list[] = $value->parse();
                } else {
                    $valid = false;
                }
            }

            if (!$valid) {
                $set = [];
            }
        }

        $this->val = $list;
    }

    /**
     * Returns this as a primitive array.
     *
     * @return array
     */
    public function parse()
    {
        return (array) $this->val;
    }

    /**
     * Alias for parse.
     *
     * @return array
     */
    public function toArray()
    {
        return $this->parse();
    }

    /**
     * Returns this as a primitive string or String.
     *
     * @param bool $primitive
     *
     * @return string|String
     */
    public function toString($primitive = false)
    {
        if ($primitive) {
            return (string) $this->val;
        }

        return new Str($this->val);
    }

    /**
     * Returns $val when cast to a string.
     *
     * Examples of usage:
     *   echo $intObject;
     *   (string) $intObject;
     *
     * @return string
     */
    public function __toString()
    {
        return $this->toString(true);
    }
}
