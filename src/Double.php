<?php

use ScalarObjects\Arithmatic;
use ScalarObjects\ObjectInterface;

/**
 * Class Double
 */
class Double implements ObjectInterface
{
    use Arithmatic;

    /**
     * @var double
     */
    private $val = 0.0;

    /**
     * @param double|Double|Integer|String $val
     */
    public function __construct($val = 0.0)
    {
        if ($val instanceof Double) {
            $val = $val->parse();
        } elseif ($val instanceof Integer) {
            $val = $val->parse();
        } elseif ($val instanceof Str) {
            $val = $val->toInteger()->parse();
        }

        $this->val = (double) $val;
    }

    /**
     * Returns this as a primitive double.
     *
     * @return double
     */
    public function parse()
    {
        return (double) $this->val;
    }

    /**
     * Returns this as a primitive int or Integer.
     *
     * @param bool $primitive
     *
     * @return int|Integer
     */
    public function toInteger($primitive = false)
    {
        if ($primitive) {
            return (int) $this->val;
        }

        return new Integer((int) $this->val);
    }

    /**
     * Alias for toInteger.
     *
     * @param bool $primitive
     *
     * @return int|Integer
     */
    public function toInt($primitive = false)
    {
        return $this->toInteger($primitive);
    }

    /**
     * Returns this as a primitive string or String.
     *
     * @param bool $primitive
     *
     * @return string|String
     */
    public function toString($primitive = false)
    {
        if ($primitive) {
            return (string) $this->val;
        }

        return new Str($this->val);
    }

    /**
     * Returns $val when cast to a string.
     *
     * Examples of usage:
     *   echo $intObject;
     *   (string) $intObject;
     *
     * @return string
     */
    public function __toString()
    {
        return $this->toString(true);
    }
}
