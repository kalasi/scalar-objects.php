<?php

use ScalarObjects\Arithmatic;
use ScalarObjects\ObjectInterface;

/**
 * Class Integer
 */
class Integer implements ObjectInterface
{
    use Arithmatic;

    /**
     * The value of this object.
     *
     * @var int
     */
    private $val = 0;

    /**
     * @param int $val
     */
    public function __construct($val = 0)
    {
        if ($val instanceof Double) {
            $val = $val->toInteger()->parse();
        } elseif ($val instanceof Integer) {
            $val = $val->parse();
        } elseif ($val instanceof Str) {
            $val = $val->toInteger()->parse();
        }

        $this->val = (int) $val;
    }

    /**
     * Alias for absolute.
     *
     * @return mixed
     */
    public function abs()
    {
        return $this->absolute();
    }

    /**
     * Sets this to an absolute number.
     *
     * @return $this
     */
    public function absolute()
    {
        $this->val = abs($this->val);

        return $this;
    }

    /**
     * Alias for toBin.
     *
     * @param bool $primitive
     *
     * @return string|Str
     */
    public function decbin($primitive = false)
    {
        return $this->toBin($primitive);
    }

    /**
     * Alias for toHex.
     *
     * @param bool $primitive
     *
     * @return string|Str
     */
    public function dechex($primitive = false)
    {
        return $this->toHex($primitive);
    }

    /**
     * Alias for toOct.
     *
     * @param bool $primitive
     *
     * @return string|Str
     */
    public function decoct($primitive = false)
    {
        return $this->toOct($primitive);
    }

    /**
     * Returns this as a primitive int.
     *
     * @return int
     */
    public function parse()
    {
        return (int) $this->val;
    }

    /**
     * Alias for power.
     *
     * @param      $exp
     * @param bool $primitive
     *
     * @return $this|double|Double|int|Integer
     */
    public function pow($exp, $primitive = false)
    {
        return $this->power($exp, $primitive);
    }

    /**
     * Raises this number by an exponential value.
     *
     * @param      $exp
     * @param bool $primitive
     *
     * @return $this|double|Double|int|Integer
     */
    public function power($exp, $primitive = false)
    {
        $result = pow($this->val, $exp);

        if ($primitive) {
            return $result;
        }

        if (is_double($result)) {
            return new Double($result);
        }

        $this->val = $result;

        return $this;
    }

    /**
     * Returns this in binary form.
     *
     * @param bool $primitive
     *
     * @return string|Str
     */
    public function toBin($primitive = false)
    {
        $val = decbin($this->val);

        if ($primitive) {
            return $val;
        }

        return new Str($val);
    }

    /**
     * Returns this in hexadecimal form.
     *
     * @param bool $primitive
     *
     * @return string|Str
     */
    public function toHex($primitive = false)
    {
        $val = dechex($this->val);

        if ($primitive) {
            return $val;
        }

        return new Str($val);
    }

    /**
     * Returns this in octal form.
     *
     * @param bool $primitive
     *
     * @return string|Str
     */
    public function toOct($primitive = false)
    {
        $val = decoct($this->val);

        if ($primitive) {
            return $val;
        }

        return new Str($val);
    }

    /**
     * Returns this as a primitive double or Double.
     *
     * @param bool $primitive
     *
     * @return double|Double
     */
    public function toDouble($primitive = false)
    {
        if ($primitive) {
            return (double) $this->val;
        }

        return new Double($this->val);
    }

    /**
     * Alias for toDouble.
     *
     * @param bool $primitive
     *
     * @return double|Double
     */
    public function toFloat($primitive = false)
    {
        return $this->toDouble($primitive);
    }

    /**
     * Returns this as a primitive string or Str.
     *
     * @param bool $primitive
     *
     * @return string|Str
     */
    public function toString($primitive = false)
    {
        if ($primitive) {
            return (string) $this->val;
        }

        return new Str($this->val);
    }

    /**
     * Returns $val when cast to a string.
     *
     * Examples of usage:
     *   echo $intObject;
     *   (string) $intObject;
     *
     * @return string
     */
    public function __toString()
    {
        return $this->toString(true);
    }
}
