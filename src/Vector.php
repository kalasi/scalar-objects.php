<?php

use ScalarObjects\ObjectInterface;

/**
 * This class is based off of Hack's collection Vector.
 *
 * Class Vector
 */
class Vector implements ObjectInterface
{
    /**
     * @var array
     */
    private $val = [];

    /**
     * @param array $integers
     */
    public function __construct($integers = [])
    {
        if (array_product(array_map('is_numeric', array_keys($integers)))) {
            $this->val = $integers;
        }
    }

    /**
     * Adds an int or Integer to this.
     *
     * @param $int
     *
     * @return $this
     */
    public function add($int)
    {
        if (is_int($int)) {
            $this->val[] .= $int;
        } elseif ($int instanceof Integer) {
            $this->val[] .= $int->parse();
        }

        return $this;
    }

    /**
     * Adds an array of ints or Integers to this.
     *
     * @param $ints
     *
     * @return $this
     */
    public function addAll($ints)
    {
        if (is_array($ints)) {
            foreach ($ints as $int) {
                if (is_integer($int)) {
                    $this->val[] .= $int;
                } elseif ($int instanceof Integer) {
                    $this->val[] .= $int->parse();
                }
            }
        }

        return $this;
    }

    /**
     * Clears all keys in this.
     *
     * @return $this
     */
    public function clear()
    {
        $this->val = [];

        return $this;
    }

    /**
     * Returns this as a primitive array.
     *
     * @return array
     */
    public function parse()
    {
        return (array) $this->val;
    }

    /**
     * Alias for parse.
     *
     * @return array
     */
    public function toArray()
    {
        return $this->parse();
    }

    /**
     * Returns this as a primitive string or String.
     *
     * @param bool $primitive
     *
     * @return string|String
     */
    public function toString($primitive = false)
    {
        $string = '';

        foreach ($this->val as $key => $val) {
            $string .= $key . ': ' . $val . ', ';
        }

        $to = max(0, strlen($string) - 2);
        $string = substr($string, 0, $to);

        if ($primitive) {
            return (string) $string;
        }

        return new Str($string);
    }

    /**
     * Returns $val when cast to a string.
     *
     * Examples of usage:
     *   echo $intObject;
     *   (string) $intObject;
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->toString(true);
    }
}
