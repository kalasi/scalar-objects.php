<?php

use ScalarObjects\ObjectInterface;

/**
 * Many of these methods are based off of Java's implementation of booleans.
 *
 * Class Boolean
 */
class Boolean implements ObjectInterface
{
    /**
     * @var bool
     */
    private $val = false;

    /**
     * Checks to see if the argument is a scalar object
     * and parses it, then setting it to field $val.
     *
     * @param bool|Boolean|Double|Integer|mixed|String $val
     */
    public function __construct($val = false)
    {
        if ($val instanceof Boolean) {
            $val = $val->parse();
        } elseif ($val instanceof Double) {
            $val = $val->parse();
        } elseif ($val instanceof Integer) {
            $val = $val->parse();
        } elseif ($val instanceof Str) {
            $val = $val->parse();
        }

        $this->val = $val;
    }

    /**
     * Compares a boolean or Boolean to this.
     * Based off of Java's implementation.
     *
     * Return -1 if this is false and the argument is true.
     * Return 0 if this and the argument are equal.
     * Return 1 if this is true and the argument is false.
     *
     * @param      $bool
     * @param bool $primitive
     *
     * @return int|Integer
     */
    public function compareTo($bool, $primitive = false)
    {
        if ($bool instanceof Boolean) {
            $bool = $bool->parse();
        } else {
            $bool = (boolean) $bool;
        }

        $ret = 0;

        if ($this->val === false && $bool === true) {
            $ret = -1;
        } elseif ($this->val === true && $bool === false) {
            $ret = 1;
        }

        if ($primitive) {
            return $ret;
        }

        return new Integer($ret);
    }

    /**
     * Returns a Boolean of true if the object is equal to this object and false if not.
     * Based off of Java's implementation.
     *
     * @param         $object
     * @param boolean $primitive
     *
     * @return bool|Boolean
     */
    public function equals($object, $primitive = false)
    {
        $converted = false;

        if (!is_null($object) && $object) {
            $converted = true;
        }

        $result = $converted === $this->val;

        if ($primitive) {
            return $result;
        }

        return new Boolean($result);
    }

    /**
     * Returns this as a primitive boolean.
     *
     * @return bool
     */
    public function parse()
    {
        return (boolean) $this->val;
    }

    /**
     * Returns this as a primitive int or Integereger.
     *
     * @param bool $primitive
     *
     * @return int|Integer
     */
    public function toInt($primitive = false)
    {
        return $this->toInteger($primitive);
    }

    /**
     * Returns this as a primitive int or Integereger.
     *
     * @param bool $primitive
     *
     * @return int|Integer
     */
    public function toInteger($primitive = false)
    {
        if ($primitive) {
            return (int) $this->val;
        }

        return new Integer($this->val);
    }

    /**
     * Returns this as a primitive string or String.
     *
     * @param bool $primitive
     *
     * @return string|String
     */
    public function toString($primitive = false)
    {
        $result = $this->val === true ? '1' : '0';

        if ($primitive) {
            return (string) $result;
        }

        return new Str($result);
    }

    /**
     * Returns $val when cast to a string.
     *
     * Examples of usage:
     *   echo $stringObject;
     *   (string) $stringObject;
     *
     * @return mixed
     */
    public function __toString()
    {
        return $this->toString(true);
    }
}
