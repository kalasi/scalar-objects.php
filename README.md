# Scalar Objects for PHP

[![ci-badge][]][ci] [![license-badge][]][license] [![docs-badge][]][docs]

> Scalar objects for PHP.

Scalar objects supports PHP's boolean, double/float, int, and string.  It also support's Hack's Map, Set, and Vector.

Here is an example of how to use scalar objects:

```php
$integer = new Integer(7);
$result = $int->multiplyBy(6); // Integer(42)
```

Note that by default methods in scalar objects will return a scalar object.
You can return primitive types by adding true to the end of the argument list:

```php
$string = new Str('This is a STRING.');
$result = $string->toLowerCase(true); // 'this is a string.'
```

Alternatively you can call `parse()` at the end of a chain:

```php
$integer = new Integer(4);
$result = $integer->add(5)->divideBy(3)->parse(); // 3
```

## Supported Scalars

The following scalar objects are supported:

### Boolean

```php
$bool = new Boolean(true);
$compare = $bool->compareTo(false); // 1
```

### Double

> **Note**: equivilant to a float.

```php
$double = new Double(1.1);
$string = $double->toString(); // Str('1.1')
```

### Integer

```php
$integer = new Integer(4);
$result = $integer->add(3)->multiplyBy(2.5); // Double(17.5)
```

### Map

```php
$map = new Map([0 => 'test', '1' => 3);
$result = $map->toArray(); // [0 => 'test', '1' => 3]
```

### Set

```php
$set = new Set([0 => 'test']);
$result = $set->toArray(); // [0 => 'test']
```

### Str

```php
$str = new Str('test.');
$result = $str->length; // 5
```

### Vector

```php
$vector = new Vector([0 => true]);
$result = $vector->toArray(); // [0 => true]
```

# License

ISC. Full license located in the [LICENSE.md] file.

[ci]: https://gitlab.com/kalasi/scalar-objects.php/pipelines
[ci-badge]: https://gitlab.com/kalasi/scalar-objects.php/badges/master/build.svg
[docs]: https://docs.austinhellyer.me/scalar-objects
[docs-badge]: https://img.shields.io/badge/docs-online-2020ff.svg
[license]: https://gitlab.com/kalasi/scalar-objects.php/blob/master/LICENSE.md
[license-badge]: https://img.shields.io/badge/License-MIT-blue.svg
